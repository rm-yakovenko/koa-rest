const Joi = require('@hapi/joi');

module.exports = Joi.object({
    username: Joi.string()
}).options({stripUnknown: true});