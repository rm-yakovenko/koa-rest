const Joi = require('@hapi/joi');

const UserRequest = Joi.object({
    username: Joi.string().trim()
        .required()    
}).required();

module.exports = UserRequest;