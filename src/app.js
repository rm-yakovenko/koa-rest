const Koa = require('koa');
const Router = require('@koa/router');
const userRequest = require('./request/userRequest');
const userListRequest = require('./request/userListRequest');
const bodyParser = require('koa-bodyparser');

const app = new Koa();
const router = new Router();

const users = [];

router.get('/', (ctx) => {
    ctx.body = 'Hello world!';
});

router.get('/users', async (ctx) => {
    let query = await userListRequest.validateAsync(ctx.request.query);
    ctx.body = {
        users: users.filter(user => user.username.indexOf(query.username) !== -1),
        query
    }
});

router.post('/users', async (ctx) => {
    let user = await userRequest.validateAsync(ctx.request.body);
    users.push(user);
    ctx.body = user;
});

app
    .use(async (ctx, next) => {
        try {
            await next();
        } catch (err) {
            if (!err.isJoi) {
                throw err;
            }
            ctx.status = 400;
            ctx.body = err.details;
        }
    })
    .use(bodyParser())
    .use(router.routes())
    .use(router.allowedMethods());

module.exports = app;